<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File ;

class CreateFeature extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feature:generate {feature?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Feature';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $args = $this->arguments();

        if (isset($args["feature"])) {
            $path = $this->viewPath(Str::plural($args["feature"]));
            
            $feature = Str::plural( Str::studly($args["feature"]));
            

            try {
                Artisan::call("datatables:make $feature");
                $this->info("DataTable $feature generated");
                Artisan::call("make:controller ".$feature."Controller");
                $this->info("Controller $feature generated");
                Artisan::call("make:model ".Str::studly($args["feature"]));
                $this->info("Model $feature generated");
                Artisan::call("make:migration createTable$feature --create=".Str::plural($args["feature"]));
                $this->info("Migration $feature generated");
                $this->createDir($path);


                File::put( $path,str_replace("FEATURE", $feature,$this->getViewContent() ) );
                
                $this->info("File {$path} created.");
                
                $ctrlContent = $this->getControllerContent();
                
                $ctrlContent = str_replace("Users",$feature, $ctrlContent);
                $ctrlContent = str_replace("users",Str::plural($args["feature"]), $ctrlContent);
                File::put( "app/Http/Controllers/".$feature."Controller.php", $ctrlContent);

                $routeContent = $this->getRouteContent();
                $routeContent  = str_replace("Users", $feature, $routeContent );
                $routeContent  = str_replace("users",Str::plural($args["feature"]), $routeContent );
                
                $routeFile = file_get_contents("routes/web.php");
                $routeFile = str_replace("	// DON'T REMOVE THIS LINE", "	$routeContent\n\n\n	// DON'T REMOVE THIS LINE", $routeFile);
                file_put_contents("routes/web.php", $routeFile);
            } catch (Exception $error) {
                $this->error($error->getMessage());
            }
            
        }

        return Command::SUCCESS;
    }

    public function viewPath($view)
    {
        $view = str_replace('.', '/', $view) . '/index.blade.php';

        $path = "resources/views/{$view}";

        return $path;
    }

    public function createDir($path)
    {
        $dir = dirname($path);

        if (!file_exists($dir))
        {
            mkdir($dir, 0777, true);
        }
    }

    public function getControllerContent() {
        return <<<EOD
        <?php
        namespace App\Http\Controllers;
        use Illuminate\Http\Request;
        use App\DataTables\UsersDataTable;

        class UsersController extends Controller
        {
            public function index(UsersDataTable \$dataTable)
            {
                return \$dataTable->render('users.index');
            }
        }
        EOD;
    }
    public function getViewContent() {
        return <<<EOD
        @extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

        @section('content')
            @include('layouts.navbars.auth.topnav', ['title' => 'FEATURE'])
                <!-- End Navbar -->
                <div class="container-fluid py-4">
                    <div class="card">
                        <div class="card-header">Manage FEATURE</div>
                        <div class="card-body table-responsive">
                            {{ \$dataTable->table() }}
                        </div>
                    </div>
                </div>
            </main>
            @push('scripts')
            <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
            <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
            <script src="/vendor/datatables/buttons.server-side.js"></script>
            {{ \$dataTable->scripts(attributes: ['type' => 'module']) }}
            @endpush
        @endsection
        EOD;

    }

    public function getRouteContent() {
        return <<<EOD
        Route::get('/users', [App\Http\Controllers\UsersController::class, 'index'])->name('users.index');
        EOD;
    }
}
