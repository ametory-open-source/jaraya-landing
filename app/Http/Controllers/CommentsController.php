<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\CommentsDataTable;
use App\Models\Comment;

class CommentsController extends Controller
{
    public function index(CommentsDataTable $dataTable)
    {
        return $dataTable->render('comments.index');
    }
    public function edit(Comment $comment)
    {
       return view("comments.edit", compact("comment"));
    }
    public function approval(Request $request, Comment $comment)
    {
        $comment->update($request->all());
        return redirect()->route("comments.index")->with('success', "Comment ".$comment->comment. " ". $request->status);
    }

    public function delete(Comment $comment)
    {
        try {
            $comment->delete();
            return redirect('comments')->with('success', "comment  Deleted");
        } catch (\Illuminate\Database\QueryException $e) {
            $message = $e->errorInfo[2];

            if (\str_contains($message, "Cannot delete or update a parent row")) {
                $message = "Comment contains replies, delete reply first";
            }
            
            return back()->with('error', $message);
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}