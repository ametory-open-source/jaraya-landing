<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\ContactusesDataTable;

class ContactusesController extends Controller
{
    public function index(ContactusesDataTable $dataTable)
    {
        return $dataTable->render('contact_uses.index');
    }
}