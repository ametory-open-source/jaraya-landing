<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\GalleriesDataTable;
use App\Models\Gallery;
use App\Models\GalleryItem;

class GalleriesController extends Controller
{
    public function index(GalleriesDataTable $dataTable)
    {
        return $dataTable->render('galleries.index');
    }

    public function create(Request $request)
    {
        return view('galleries.create');
    }

    public function edit(Request $request, Gallery $gallery)
    {
        return view('galleries.edit', compact("gallery"));
    }

    public function update(Request $request, Gallery $gallery)
    {
        $input = $request->except(["_token", "files"]);

        try {
            if ($request->has("picture") && is_array($request->get("picture"))) {
                foreach ($request->get("picture") as $key => $picture) {
                    $caption = $request->get("caption")[$key];
                    if (isset($request->get("item_id")[$key])) {
                        $galleryItem = GalleryItem::find($request->get("item_id")[$key]);
                        $galleryItem->update([
                            "picture" => $picture,
                            "description" => $caption ?? "",
                            "gallery_id" => $gallery->id,
                        ]);
                    } else {
                        GalleryItem::create([
                            "picture" => $picture,
                            "description" => $caption ?? "",
                            "is_cover" => $key == 0,
                            "gallery_id" => $gallery->id,
                        ]);
                    }
                    
                }
            }

            $gallery->update($input);
            return back()->with('success', "Gallery ".$request->get("title"). " Updated");
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
        
    }

    public function cover(Request $request, Gallery $gallery)
    {
        \DB::enableQueryLog();
        $galleryItem = GalleryItem::find($request->item_id);
        GalleryItem::query()->where('gallery_id', '=', $gallery->id)->update(['is_cover' => false]);
        $galleryItem->is_cover = true;
        $galleryItem->save();
        // dd(\DB::getQueryLog());
        return back()->with('success', "Gallery Cover Updated");
    }

    public function deleteItem(Request $request, Gallery $gallery)
    {
        \DB::enableQueryLog();
        $galleryItem = GalleryItem::find($request->item_id)->delete();
        if(GalleryItem::where("is_cover", true)->count() == 0 && GalleryItem::count()) {
            GalleryItem::first()->update(["is_cover" => true]);
        }
        // dd(\DB::getQueryLog());
        return back()->with('success', "Gallery Item Deleted");
    }

    public function store(Request $request)
    {
        try {
            $input = $request->except(["_token", "files"]);
            $input = collect($input)->map(function($d) {
                if (!$d) return "";
                return $d;
            })->toArray();
            $gallery = Gallery::create($input);
            return redirect()->route("galleries.edit", ["gallery" => $gallery])->with('success', "Gallery ".$request->get("title"). " Created");
        } catch (\Throwable $th) {  
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
