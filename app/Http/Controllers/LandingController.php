<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Web;
use App\Models\Module;
use App\Models\Newsletter;
use App\Models\Page;
use App\Models\Blog;
use App\Models\Gallery;
use App\Models\BlogCategory;
use App\Models\Comment;
use App\Models\ContactUs;
use Vedmant\FeedReader\Facades\FeedReader;
use Illuminate\Support\Facades\Http;

class LandingController extends Controller
{
    public function main()
    {
        $web = Web::first();

        $rss = [];
        // if ($web->blog_rss_url) {
        //     $f = FeedReader::read($web->blog_rss_url);
        //     foreach ($f->get_items() as $key => $item) {
        //         $rss[$key]["title"] = $item->get_title();
        //         $rss[$key]["description"] = $item->get_content();
        //         $rss[$key]["short"] = limit_text(strip_tags($item->get_content()), 20);
        //         $rss[$key]["link"] = $item->get_link();
        //         preg_match('/<img(?: [^<>]*?)?src=([\'"])(.*?)\1/', $item->get_content(), $matches);
        //         $rss[$key]["img"] = ($matches) ?  end($matches) : null;
        //         # code...
        //     }
        // }

        // $rss = array_slice($rss, 0, 5);

        $growth_modules = Module::where("type", "GROWTH")->where('flag', true)->orderBy("sort")->get();
        $features = Module::where("type", "FEATURE")->where('flag', true)->orderBy("sort")->get();
        $steps = Module::where("type", "STEP")->where('flag', true)->orderBy("sort")->get();
        $clients = Module::where("type", "CLIENT")->where('flag', true)->orderBy("sort")->get();
        $client_logos = Module::where("type", "CLIENT-LOGO")->where('flag', true)->orderBy("sort")->get();
        $questions = Module::where("type", "QUESTION")->where('flag', true)->orderBy("sort")->limit(7)->get();
        $pricings = Module::where("type", "PRICING")->where('flag', true)->orderBy("sort")->get();
        $screenshots = Module::where("type", "SCREENSHOT")->where('flag', true)->orderBy("sort")->get();

        $blogs = Blog::where("status", "PUBLISHED")->limit(2)->orderBy("blogs.created_at", "desc")->get();

        return view("home", compact("web", "rss", "growth_modules", "features", "steps", "clients", "client_logos", "questions", "pricings", "screenshots", "blogs"));
    }

    public function save_template(Request $request)
    {
        try {
            $input = $request->except(["_token"]);

            $input["meta"] = $input["meta"] ?? "";
            $input["styles"] = $input["styles"] ?? "";
            $input["scripts"] = $input["scripts"] ?? "";
            $input["footer_explore_link"] = $input["footer_explore_link"] ?? "";
            $input["footer_help_link"] = $input["footer_help_link"] ?? "";
            $input["header_menu"] = $input["header_menu"] ?? "";
            $input["name"] = $input["name"] ?? "";
            $input["short_description"] = $input["short_description"] ?? "";
            $input["description"] = $input["description"] ?? "";
            $input["meta"] = $input["meta"] ?? "";
            $input["styles"] = $input["styles"] ?? "";
            $input["scripts"] = $input["scripts"] ?? "";
            $input["playstore_link"] = $input["playstore_link"] ?? "";
            $input["appstore_link"] = $input["appstore_link"] ?? "";
            $input["hero_title"] = $input["hero_title"] ?? "";
            $input["hero_description"] = $input["hero_description"] ?? "";
            $input["feature_title"] = $input["feature_title"] ?? "";
            $input["video_link"] = $input["video_link"] ?? "";
            $input["growth_title"] = $input["growth_title"] ?? "";
            $input["growth_started_link"] = $input["growth_started_link"] ?? "";
            $input["growth_learn_link"] = $input["growth_learn_link"] ?? "";
            $input["step_title"] = $input["step_title"] ?? "";
            $input["step_started_link"] = $input["step_started_link"] ?? "";
            $input["step_learn_link"] = $input["step_learn_link"] ?? "";
            $input["client_title"] = $input["client_title"] ?? "";
            $input["client_subtitle"] = $input["client_subtitle"] ?? "";
            $input["client_description"] = $input["client_description"] ?? "";
            $input["question_title"] = $input["question_title"] ?? "";
            $input["pricing_title"] = $input["pricing_title"] ?? "";
            $input["screenshot_title"] = $input["screenshot_title"] ?? "";
            $input["blog_title"] = $input["blog_title"] ?? "";
            $input["blog_rss_url"] = $input["blog_rss_url"] ?? "";
            $input["blog_url"] = $input["blog_url"] ?? "";
            $input["newsletter_title"] = $input["newsletter_title"] ?? "";
            $input["offline_template"] = $input["offline_template"] ?? "";

            $pictures = [
                "logo",
                "loading",
                "hero_picture",
                "video_picture",
                "question_picture",
                "subscribe_picture",
            ];

            foreach ($pictures as $key => $pic) {
                if ($request->has($pic)) {
                    $path = $request->file($pic)->store('public/images', 'local');
                    $input[$pic] = str_replace("public", "", $path);
                }
            }


            $input["copyright"] = $input["copyright"] ?? "";


            $input["show_header"] = isset($input["show_header"]) ? true : false;
            $input["show_hero"] = isset($input["show_hero"]) ? true : false;
            $input["show_feature"] = isset($input["show_feature"]) ? true : false;
            $input["show_video"] = isset($input["show_video"]) ? true : false;
            $input["show_growth"] = isset($input["show_growth"]) ? true : false;
            $input["show_step"] = isset($input["show_step"]) ? true : false;
            $input["show_client"] = isset($input["show_client"]) ? true : false;
            $input["show_question"] = isset($input["show_question"]) ? true : false;
            $input["show_pricing"] = isset($input["show_pricing"]) ? true : false;
            $input["show_screenshot"] = isset($input["show_screenshot"]) ? true : false;
            $input["show_blog"] = isset($input["show_blog"]) ? true : false;
            $input["show_newsletter"] = isset($input["show_newsletter"]) ? true : false;
            $input["show_footer"] = isset($input["show_footer"]) ? true : false;
            $input["is_online"] = isset($input["is_online"]) ? true : false;
            $web = Web::first();
            if ($web) {
                $web->update($input);
            } else {
                Web::create($input);
            }
            return back()->with("success", "Template Updated");
        } catch (\Throwable $th) {
            dd($th);
        }


    }

    public function upload(Request $request)
    {
        $path = $request->file('file')->store('public/images', 'local');
        $data = [
            "file" => str_replace("public", "", $path),
            "url" => assetUrl(str_replace("public", "", $path)),
        ];
        return response()->json($data) ;
    }

    public function template()
    {
        $web = Web::first();
        if (!$web) $web = new Web();
        return view("template", compact("web"));
    }

    public function page(Request $request, $slug)
    {
        $web = Web::first();
        $page = Page::where("slug", $slug)->where("status", "PUBLISHED")->firstOrFail();

        return view('static.page', compact("page", "web"));
    }

    public function gallery(Request $request, Gallery $gallery)
    {
        $web = Web::first();
        return view('static.gallery', compact("gallery", "web"));
    }

    public function galleryList(Request $request)
    {

        $web = Web::first();
        $galleries = Gallery::where("status", "PUBLISHED")->orderBy("galleries.created_at", "desc")->paginate();

        return view("static.galleryList", compact("web", "galleries"));
    }

    public function contact(Request $request)
    {

        $web = Web::first();

        return view("static.contact", compact("web"));
    }
    public function contactSend(Request $request)
    {

        try {
            $input = $request->except(["_token"]);
            ContactUs::create($input);
            return back()->with("success", "Thank you for your participation");
        } catch (\Throwable $th) {

            // dd($th);
            return back()->with("error", "Something error")->withInput();
        }
    }
    public function blogList(Request $request)
    {
        $web = Web::first();
        $blogs = Blog::where("status", "PUBLISHED")->when($request->has('category') && $request->get('category') != "", function($q) use($request) {
            $q->join('blog_categories', 'blogs.blog_category_id', '=', 'blog_categories.id')->where("blog_categories.name", $request->get("category"));
        })->when($request->has('search') && $request->get('search') != "", function($q) use($request) {
            $q->where(function($q)  use($request)  {
                $q->where("title", "like", "%".$request->search."%");
                $q->orWhere("description", "like", "%".$request->search."%");
            });
        })->orderBy("blogs.created_at", "desc")->paginate();

        $categories = BlogCategory::all();


        return view("static.blogList", compact("web", "blogs", "categories"));
    }
    public function blog(Request $request, $slug)
    {
        $web = Web::first();
        $blog = Blog::where("slug", $slug)->where("status", "PUBLISHED")->firstOrFail();

        if ($blog->short_description) {
            $web->description = $blog->short_description;
        }
        if ($blog->feature_image) {
            $web->image = assetUrl($blog->feature_image);
        }
        if ($blog->keywords) {
            $web->keywords = $blog->keywords;
        }

        $web->name .= " | ". $blog->title;


        return view('static.blog', compact("blog", "web"));
    }

    public function postComment(Request $request, Blog $blog)
    {
        try {
            $input = $request->except(["_token"]);
            $input["status"] = $blog->comment_moderation ? "DRAFT" : "PUBLISHED";
            $msg = $blog->comment_moderation ? "Your comment will appear after being moderated by the admin" : "Thank you for your participation
            ";
            $input["blog_id"] = $blog->id;
            Comment::create($input);
            return back()->with("success", $msg);
        } catch (\Throwable $th) {

            // dd($th);
            return back()->with("error", "Something error")->withInput();
        }

    }

    public function newsletter(Request $request)
    {
        try {
            Newsletter::create($request->all());
            return response()->json(['status' => 'success', 'message' => "Terima  Kasih Telah Berlangganan"]);
        } catch (\Illuminate\Database\QueryException $e) {
            $message = $e->errorInfo[2];

            if (str_contains($message, "Duplicate entry")) {
                $message = "EMAIL SUDAH TERDAFTAR";
            }
            return response()->json(['status' => 'failed', 'message' => $message]);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'failed', 'message' => "Berlanganan Gagal"]);
        }

    }

    public function pricing()
    {
        $web = Web::first();
        $pricings = Module::where("type", "PRICING")->where('flag', true)->get();

        return view("static.pricing", compact("web", "pricings"));
    }

    public function faq()
    {
        $web = Web::first();
        $questions = Module::where("type", "QUESTION")->where('flag', true)->get();

        return view("static.faq", compact("web", "questions"));
    }

    public function post_auth_login(Request $request)
    {

        try {
            $response = Http::post(env("APP_BASE_URL")."/api/v2/Login", [
                'username' => $request->email,
                'password' => $request->password,
                'device' => "web"
            ]);

            if ($response->status() == 200) {
                $resp = $response->json();
                $token = $resp["token"];
                $default_company_id = $resp["default_company_id"];

                $link = env('APP_FRONTEND_URL')."/login?company_id=".$default_company_id."&token=".$token;
                return back()->with("redirect", $link)->withInput();
            }
        } catch (\Throwable $th) {
            return back()->with("error", "Something error")->withInput();
        }
    }

    public function auth_login()
    {
        $web = Web::first();


        // return view("static.login", compact("web"));
        return  redirect()->to("https://app.jaraya.id/login");
    }
    public function post_join(Request $request)
    {
        $input = $request->except("_token");

        // dd($input);
        try {
            $response = Http::post(env("APP_BASE_URL")."/api/v2/Join", $input);
            if ($response->status() == 200) {
                return back()->with("success", "Your submission will be reviewed by our admin, please wait for the confirmation email")->withInput();
            } else {
                throw new Exception($response->body());
            }
        } catch (\Throwable $th) {
            return back()->with("error", "Something error")->withInput();
        }
    }
    public function invitation(Request $request)
    {
        $web = Web::first();
        return view("static.join", compact("web"));
    }
    public function join(Request $request)
    {
        $web = Web::first();
        if ($request->has("affiliate_code")) {
            return redirect()->to("https://app.jaraya.id/register?affiliate_code=".$request->get("affiliate_code"));
        }

        return  redirect()->to("https://app.jaraya.id/register");
        // return view("static.join", compact("web"));
    }



}
