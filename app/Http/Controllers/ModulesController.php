<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\ModulesDataTable;
use App\Models\Module;

class ModulesController extends Controller
{
    public function index(ModulesDataTable $dataTable)
    {
        return $dataTable->render('modules.index');
    }
    public function create(Request $request)
    {
        return view("modules.create");
    }
    public function store(Request $request)
    {
        try {
            if ($request->type == "FEATURE" || $request->type == "STEP" || $request->type == "CLIENT") {
                $count = Module::where('type', $request->type)->where('flag', true)->count();
                if ($count >= 4) {
                    throw new \Exception("Module Limited");
                }
            }
            if ($request->type == "STEP" || $request->type == "PRICING") {
                $count = Module::where('type', $request->type)->where('flag', true)->count();
                if ($count >= 3) {
                    throw new \Exception("Module Limited");
                }
            }
            // if ($request->type == "QUESTION") {
            //     $count = Module::where('type', $request->type)->where('flag', true)->count();
            //     if ($count >= 7) {
            //         throw new \Exception("Module Limited");
            //     }
            // }
            $input = $request->except(["_token"]);
            if (!$request->get("description")) {
                $input["description"] = ' ';
            }
            Module::create($input);
            return redirect('modules');
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

    }
    public function edit(Module $module)
    {
        try {

            return view("modules.edit", compact("module"));
        } catch (\Throwable $th) {
            dd($th);
        }

    }
    public function update(Request $request, Module $module)
    {
        try {
            $input = $request->except(["_token", "_method"]);
            $input["description"] = $input["description"] ?? "";
            $module->update($input);
            return redirect('modules');
        } catch (\Throwable $th) {
            dd($th);
        }

    }
    public function delete(Module $module)
    {
        try {
            $module->delete();
            return redirect('modules');
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

    }
}
