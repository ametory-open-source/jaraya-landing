<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\StaticsDataTable;
use App\Models\StaticModel;
use App\Models\Web;


class StaticsController extends Controller
{
    public function index(StaticsDataTable $dataTable)
    {
        return $dataTable->render('statics.index');
    }

    public function create(Request $request)
    {
        return view('statics.create');
    }

    public function edit(Request $request, StaticModel $static)
    {
        return view('statics.edit', compact("static"));
    }
    
    public function store(Request $request)
    {
        try {
            $input = $request->except(["_token", "files"]);
            $input = collect($input)->map(function($d) {
                if (!$d) return "";
                return $d;
            })->toArray();
            $static = StaticModel::create($input);
            return redirect()->route("statics.edit", ["static" => $static])->with('success', "Static Feature ".$request->get("name"). " Created");
        } catch (\Throwable $th) {  
            return back()->with('error', $th->getMessage())->withInput();
        }
    }

    public function update(Request $request, StaticModel $static)
    {
        
        try {
            $input = $request->except(["_token", "files"]);
            $input = collect($input)->map(function($d) {
                if (!$d) return "";
                return $d;
            })->toArray();
            $static->update($input);
            return back()->with('success', "Static Feature ".$request->get("name"). " Updated");
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
        
    }

    public function delete(StaticModel $static)
    {
        try {
            $static->delete();
            return redirect('statics');
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
        
    }
    
    public function viewPage(string $page)
    {
        $web = Web::first();
        $static = StaticModel::where("name", $page)->first();
        if (!$static) {
            return abort(404);
        }

        return view('statics.view', compact("web","static"));
    }


    
}