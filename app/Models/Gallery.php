<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use HasFactory;

    protected $fillable = [
        "title",
        "description",
        "status",
        "keywords",
        "tags",
    ];


    public function items()
    {
        return $this->hasMany(GalleryItem::class);
    }

    public function getCoverAttribute() {
        $cover = $this->items->where("is_cover", true)->first();
        if ($cover) {
            return $cover->picture;
        }
        return "";
    }
}
