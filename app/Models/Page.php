<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $fillable = [
        "title",
        "slug",
        "feature_image",
        "feature_image_caption",
        "keywords",
        "short_description",
        "description",
        "status",
    ];
}



