<?php

namespace App\Providers;
use App\Models\Web;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('errors::404', function ($view) {
            $web = Web::first();
            $view->with('web', $web);
        });

    }
}
