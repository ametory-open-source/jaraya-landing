<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->string("company_name")->nullable()->default(null);
            $table->string("company_phone")->nullable()->default(null);
            $table->string("company_email")->nullable()->default(null);
            $table->string("company_web")->nullable()->default(null);
            $table->string("company_coordinate")->nullable()->default(null);
            $table->text("company_address")->nullable()->default(null);
            $table->text("company_map")->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            //
        });
    }
};
