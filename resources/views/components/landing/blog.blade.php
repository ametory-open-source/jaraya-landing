@if ($blogs->count())
<!--related blog section start-->
<section class="related-blog blog">
    <div class="related-blog__wrapper">
        <h2 class="section-heading color-black">{!! $web->blog_title !!}</h2>
        <div class="blog__content">
            <div class="container">
                <div class="row">
                    @foreach ($blogs as $key => $item)
                    @php
                        $class = "col-lg-4";
                    @endphp
                    @switch(fmod($key + 1, 4))
                        @case(2)
                        @php
                            $class = "col-lg-8";
                        @endphp
                            @break
                        @case(3)
                        @php
                            $class = "col-lg-8 left";
                        @endphp

                            @break
                        @default
                    @endswitch
                    <div class="{{ $class }}">
                        <a href="/blog/{{ $item->slug}}">
                            <div class="blog__single blog__single--{{ $key+1 }}">
                                <div class="blog__single-image">
                                    <img src="{{ assetUrl($item->feature_image) }}" alt="image">
                                </div>
                                <div class="blog__single-info">
                                    <h3>{{ $item->title }}</h3>
                                    <h4>{{ $item->all_comments_published->count() }} <i class="fad fa-comment"></i><span>|</span>{{
                                        $item->created_at->format("d-m-Y") }}</h4>
                                    <p class="paragraph dark">{{ $item->short_description}} </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
        <a href="/blog" class="button">
            <span>GO TO BLOG <i class="fad fa-long-arrow-right"></i></span>
        </a>
    </div>
</section>
<!--related blog section end-->
@endif
