
<!--client section start-->
<section class="clients-sec" id="feedback">
    <div class="container">
        <h2 class="section-heading color-black">{!! $web->client_title !!}</h2>
        <div class="testimonial__wrapper">
            @foreach ($clients as $i => $item)
            <div class="client client-01 {{ $i == 0 ? 'active' : null}}">
                <div class="image">
                    <img src="{{ assetUrl($item->picture) }}" alt="image">
                </div>
                <div class="testimonial">
                    <div class="testimonial__wrapper">
                        <p>“{{ $item->description }}”</p>
                        <h4>— {{ $item->title }}</h4>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="clients">
            <div class="clients__info">
                <h3>{!! $web->client_subtitle !!}</h3>
                <p class="paragraph dark">{!! $web->client_description !!}</p>
            </div>
            <div class="swiper-container clients-slider">
                <div class="swiper-wrapper">
                    @foreach ($client_logos as $item)
                    <div class="swiper-slide clients-slide">
                        <a href="{{ $item->link }}"><img style="max-width: 200px" src="{{ assetUrl($item->picture) }}" alt="{{ $item->title }}"></a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
    <div class="blog" style="padding: 3rem">
        <a href="/join{!! request()->has('affiliate_code') ? '?affiliate_code='.request()->get('affiliate_code') : null !!}" class="button" style="    min-width: 32rem">
            <span>DAPATKAN AKSES GRATIS <i class="fad fa-long-arrow-right"></i></span>
        </a>
    </div>
</section>
<!--client section end-->
