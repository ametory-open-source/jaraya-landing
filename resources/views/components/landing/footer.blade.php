
  <!--footer start-->
  <footer class="footer">
    <div class="footer__wrapper">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="footer__info">
              <div class="footer__info--logo">
                <img style="max-width: 180px" src="{{ $web->logo ? assetUrl($web->logo) : '/assets/images/logo_jaraya.png' }}" alt="image">
              </div>
              <div class="footer__info--content">
                <p class="paragraph dark">{!! $web->short_description !!}</p>
                <div class="social">
                  <ul>
                    <li class="facebook"><a target="_blank" href="https://www.facebook.com/jaraya.id"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="twitter"><a target="_blank" href="https://www.instagram.com/jaraya.app/"><i class="fab fa-instagram"></i></a></li>
                    <li class="linkedin"><a target="_blank" href="https://www.linkedin.com/company/jaraya"><i class="fab fa-linkedin-in"></i></a></li>
                    <li class="youtube"><a target="_blank" href="https://www.youtube.com/@JarayaApps"><i class="fab fa-youtube"></i></a></li>
                    <li class="tiktok"><a target="_blank" href="https://www.tiktok.com/@jaraya.app">
                        <img width="12" src="/img/tiktok.png" alt="">
                    </a></li>
                  </ul>
                </div>
                <div class="mt-5">
                    <a  target="_blank" href="https://pse.kominfo.go.id/tdpse-detail/15206">
                        <img width="100" src="/img/pse-terdaftar.png" alt="">
                    </a>
                  </div>
              </div>
            </div>
          </div>
          <div class="col-lg-8">
            <div class="footer__content-wrapper">
              <div class="footer__list">
                <ul>
                  <li>Explore</li>
                    {!! $web->footer_explore_link !!}
                </ul>
              </div>
              <div class="footer__list">
                <ul>
                  <li>Help</li>
                  {!! $web->footer_help_link !!}
                </ul>
              </div>
              <div class="download-buttons">
                <h5>Download</h5>
                <a target="_blank" href="{!! $web->playstore_link !!}" class="google-play">
                  <i class="fab fa-google-play"></i>
                  <div class="button-content">
                    <h6>GET IT ON <span>Google Play</span></h6>
                  </div>
                </a>
                <a target="_blank" href="{!! $web->appstore_link !!}" class="apple-store">
                  <i class="fab fa-apple"></i>
                  <div class="button-content">
                    <h6>GET IT ON <span>Apple Store</span></h6>
                  </div>
                </a>

              </div>


            </div>
          </div>
        </div>
        <div class="row">
          <div class="footer__copy">
            <h6>&copy; PT. JAYALAH RAKYAT INDONESIA & DEVELOPED BY BISKOD DEV TEAM</h6>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--footer end-->
