<!--growth section start-->
<section class="growth" id="feature">
    <div class="growth__wrapper">
        <div class="container">
            <h2 class="section-heading color-black">{!! $web->growth_title !!}</h2>
            <div class="row">
                @foreach ($growth_modules as $item)
                <div class="col-lg-6">
                    <div class="growth__box">
                        <div class="icon">
                            <i class="fad {{ $item->icon }}"></i>
                        </div>
                        <div class="content">
                            <h3>{!! $item->title !!}</h3>
                            <p class="paragraph dark">{!! $item->description !!}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row">
                <div class="button__wrapper">
                    <a href="{!! $web->growth_started_link !!}" class="button">
                        <span>MULAI SEKARANG <i class="fad fa-long-arrow-right"></i></span>
                    </a>
                    <a href="{!! $web->growth_learn_link !!}" class="button">
                        <span>COBA DEMO <i class="fad fa-long-arrow-right"></i></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--growth section end-->
