
  <!--header section start-->
  <header class="header header-1">
    <div class="container">
      <div class="header__wrapper">
        <div class="header__logo">
          <a href="/">
            <img style="max-width: 180px" src="{{ $web->logo ? assetUrl($web->logo) : '/assets/images/logo_jaraya.png' }}" alt="logo">
          </a>
        </div>
        <div class="header__nav">
          <ul class="header__nav-primary">
            {!! $web->header_menu !!}
          </ul>
          <span><i class="fas fa-times"></i></span>
        </div>
        <div class="header__bars">
          <div class="header__bars-bar header__bars-bar-1"></div>
          <div class="header__bars-bar header__bars-bar-2"></div>
          <div class="header__bars-bar header__bars-bar-3"></div>
        </div>
      </div>
    </div>
  </header>
  <!--header section end-->