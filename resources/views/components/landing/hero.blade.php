<!--hero section start-->
<section class="hero">
    <div class="hero__wrapper">
        <div class="container">
            <div class="row align-items-lg-center">
                <div class="col-lg-6 order-2 order-lg-1">
                    <h1 class="main-heading color-black">{!! $web->hero_title !!}</h1>
                    <p class="paragraph">{!! $web->hero_description !!}</p>
                    <div class="download-buttons">
                        <a target="_blank" href="{!! $web->playstore_link !!}" class="google-play">
                            <i class="fab fa-google-play"></i>
                            <div class="button-content">
                                <h6>GET IT ON <span>Google Play</span></h6>
                            </div>
                        </a>
                        <a target="_blank" href="{!! $web->appstore_link !!}" class="apple-store">
                            <i class="fab fa-apple"></i>
                            <div class="button-content">
                                <h6>GET IT ON <span>Apple Store</span></h6>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 order-1 order-lg-2">
                    <div class="questions-img hero-img">
                        @if($web->hero_picture)
                            <img src="{{ asset('/storage'. $web->hero_picture) }}" alt="image">
                        @else
                            <img src="/assets/images/phone-01.png" alt="image">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--hero section end-->