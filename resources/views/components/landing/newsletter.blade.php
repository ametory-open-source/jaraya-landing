<!--newsletter section start-->
<section class="newsletter">
    <div class="newsletter__wrapper">
        <div class="container">
            <div class="row align-items-lg-center">
                <div class="col-lg-6">
                    <div class="newsletter__info">
                        <h2 class="section-heading color-black">{!! $web->newsletter_title !!}</h2>
                        <form id="form" action="/newsletter" method="post" class="newsletter__info--field">
                            @csrf
                            <input type="email" id="email" name="email" placeholder="Email address" class="input-field">
                            <button class="button"><span>SUBSCRIBE <i
                                        class="fad fa-long-arrow-right"></i></span></button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="newsletter__img">
                        @if($web->subscribe_picture)
                        <img src="{{ asset('/storage'. $web->subscribe_picture) }}" alt="image">
                        @else
                        <img src="/assets/images/newsletter-img.png" alt="image">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--newsletter section end-->

@push("scripts")
<script>
    $(function() {
            $("#form").submit(function (event) {
                var formData = {
                    email: $("#email").val(),
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                };
                $.ajax({
                    type: "POST",
                    url: "/newsletter",
                    data: formData,
                    dataType: "json",
                    encode: true,
                }).done(function (data) {
                    alert(data.message);
                    $("#email").val("")
                });

                event.preventDefault();
            });
        })
</script>
@endpush