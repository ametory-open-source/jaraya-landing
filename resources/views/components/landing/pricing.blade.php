<!--pricing section start-->
<section class="pricing" id="pricing">
    <div class="pricing__wrapper">
        <h2 class="section-heading color-black">{!! $web->pricing_title !!}</h2>
        <div class="container">
            <div class="row">
                @foreach ($pricings as $i => $item)
                <div class="col-lg-4">
                    <div class="pricing__single pricing__single-{{ $i+1 }}">
                        <div class="icon">
                            <i class="fad {{ $item->icon }}"></i>
                        </div>
                        <h4>{{ $item->title }}</h4>
                        <h3><span style="font-size: 12pt">IDR</span>{{ $item->price }}</h3>
                        <h6>{{ $item->subtitle }}</h6>
                        <div class="list">
                            <ul>
                                {!! parsePricing($item->description) !!}
                            </ul>
                        </div>
                        <a href="{{ $item->link }}" class="button">
                            <span>GET STARTED <i class="fad fa-long-arrow-right"></i></span>
                        </a>
                    </div>
                </div>
                @endforeach

                
            </div>
        </div>
    </div>
</section>
<!--pricing section end-->