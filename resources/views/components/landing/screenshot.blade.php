
<!--screenshot section start-->
<section class="screenshot" id="preview">
    <div class="screenshot__wrapper">
        <div class="container">
            <div class="screenshot__info">
                <h2 class="section-heading color-black">{!! $web->screenshot_title !!}</h2>
                <div class="screenshot-nav">
                    <div class="screenshot-nav-prev"><i class="fad fa-long-arrow-left"></i></div>
                    <div class="screenshot-nav-next"><i class="fad fa-long-arrow-right"></i></div>
                </div>
            </div>
        </div>
        <div class="swiper-container screenshot-slider">
            <div class="swiper-wrapper">
                @foreach ($screenshots as $item)
                <div class="swiper-slide screenshot-slide">
                    <img src="{{ assetUrl($item->picture) }}" alt="image">
                </div>    
                @endforeach
            </div>
        </div>
    </div>
</section>
<!--screenshot section end-->