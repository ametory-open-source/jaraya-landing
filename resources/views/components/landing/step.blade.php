<!--step section start-->
<section class="step">
    <div class="step__wrapper">
        <div class="container">
            <h2 class="section-heading color-black">{!! $web->step_title !!}</h2>
            <div class="row">
                @foreach ($steps as $item)
                <div class="col-lg-4">
                    <div class="step__box">
                        <div class="image">
                            <img src="{{ assetUrl($item->picture) }}" alt="image">
                        </div>
                        <div class="content">
                            <h3>{{ $item->title }}<span>{{ $item->subtitle }}</span></h3>
                            <p class="paragraph dark">{{ $item->description }}</p>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <div class="row">
                <div class="button__wrapper">
                    <a href="{{ $web->started_link }}" class="button">
                        <span>GET STARTED <i class="fad fa-long-arrow-right"></i></span>
                    </a>
                    <a href="{{ $web->learn_link }}" class="button">
                        <span>LEARN MORE <i class="fad fa-long-arrow-right"></i></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--step section end-->