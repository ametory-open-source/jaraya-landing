@extends("layouts.landing")

@section('content')
@includeWhen($web->show_hero, 'components.landing.hero')
@includeWhen($web->show_feature, 'components.landing.feature')
@includeWhen($web->show_video, 'components.landing.video')
@includeWhen($web->show_growth, 'components.landing.growth')
@includeWhen($web->show_step, 'components.landing.step')
@includeWhen($web->show_client, 'components.landing.client')
@includeWhen($web->show_question, 'components.landing.question')
@includeWhen($web->show_pricing, 'components.landing.pricing')
@includeWhen($web->show_screenshot, 'components.landing.screenshot')
@includeWhen($web->show_blog, 'components.landing.blog')
@includeWhen($web->show_newsletter, 'components.landing.newsletter')


@endsection