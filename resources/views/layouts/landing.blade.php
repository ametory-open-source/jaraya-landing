@if($web->is_online)
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>{!! $web->name !!}</title>
  <meta name="description" content="{!! $web->description !!}">
  <meta name="title" content="{!! $web->name !!}">
  <meta name="keywords" content="{!! $web->keywords !!}">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta property="og:type" content="website">
  <meta property="og:title" content="{!! $web->name !!}" />
  <meta property="og:description" content="{!! $web->description !!}" />
  <meta property="og:site_name" content="{!! $web->short_description !!}">
  <meta property="og:url" content="{{ url("/") }}">
  @if($web->image)
  <meta property="og:image" content="{!! $web->image !!}" />
  @endif
  <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">

  <!--stylesheet-->
  <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="/assets/css/all.min.css">
  <link rel="stylesheet" href="/assets/css/swiper-bundle.min.css">
  <link rel="stylesheet" href="/assets/css/style.css">
  <style>
  {!! $web->styles !!}
  </style>

  <link href='https://fonts.googleapis.com/css?family=Ubuntu&subset=cyrillic,latin' rel='stylesheet' type='text/css' />
  <style type="text/css" >
      body {
          font-family : 'Ubuntu', sans-serif !important;
      }
      .blog_single h1, .blog_single h2, .blog_single h3 {
        letter-spacing:  0 !important;
      }
      .blog_single ol, .blog_single ul {
        padding-left: 20px;
        margin-top: 20px !important;
      }
      .blog_single li {
        font-size: 1.8rem;
        margin-bottom: 10px;
      }
      .blog_single li:last-child {
        margin-bottom: 0;
      }

      .blog_single__thumbnail img {
          width: 100%;
          border-radius: 40px;
      }
      .pricing .section-heading {
          width: 66%;
      }
      .float{
        position:fixed;
        width:60px;
        height:60px;
        bottom:40px;
        right:40px;
        background-color:#25d366;
        color:#FFF;
        border-radius:50px;
        text-align:center;
        font-size:30px;
        box-shadow: 2px 2px 3px #999;
        z-index:100;
      }

      .my-float{
        margin-top:16px;
      }

      .social ul li.tiktok {
            background: #270112;
            background: -webkit-gradient(linear, left bottom, left top, from(#270112), color-stop(50%, #rgb(159, 87, 110)), to(#270112));
            background: linear-gradient(0deg, #270112 0%, #rgb(159, 87, 110) 50%, #270112 100%);
            filter: drop-shadow(0px 5px 5px rgba(245,69,144,0.4));
            -webkit-filter: drop-shadow(0px 5px 5px rgba(245,69,144,0.4));
            -moz-filter: drop-shadow(0px 5px 5px rgba(245,69,144,0.4));
            -webkit-transform: var(--transform-fix);
            transform: var(--transform-fix);
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            -webkit-perspective: 1000;
            perspective: 1000;
        }
        .blog_single__content>*:not(.grid) {
            list-style: decimal-leading-zero;
        }
        .clients-sec .testimonial__wrapper .client .testimonial p {
            font-weight: 500;
            font-size: 1.6rem !important;
        }
        .clients-sec .testimonial__wrapper .client .image img {
            border-radius: 30px !important;
        }
        @media (max-width: 575.98px) {
            .hero .hero-img::before {
                -webkit-transform: scale(1) translate(-50%, 0) !important;
                transform: scale(1) translate(-50%, 0) !important;
                left: 50% !important;
                top: 10% !important;
                width: 20rem;
                height: 20rem;
            }
            .clients-sec .testimonial__wrapper .client .testimonial p {
                font-weight: 500;
                font-size: 1.2rem !important;
            }
            .clients-sec .testimonial__wrapper .client {
                padding: 10px;
            }
            .clients-sec .testimonial__wrapper .client .image img {
                width: 100%;
            }
            .clients-sec .testimonial__wrapper .client .testimonial p {
                font-weight: 500;
                font-size: 1.4rem !important;
            }
        }
        @media (max-width: 991.98px) {
            .clients-sec .testimonial__wrapper .client:nth-child(n+3) {
                margin-top: 60.8rem;
            }
            .clients-sec .testimonial__wrapper .client .testimonial {
                height: 50rem;
            }
        }

  </style>
  @stack("css")
</head>

<body>

  <!--preloader start-->
  <div class="preloader">
    <img src="{{ $web->loading ? assetUrl($web->loading) : '/assets/images/loading-logo.png' }}"  alt="image">
  </div>
  <!--preloader end-->

  @yield('content')
  @include("components.landing.header")
  @include("components.landing.footer")

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <a href="{!! $web->growth_started_link  ?? '' !!}" class="float" target="_blank">
  <i class="fa fa-whatsapp my-float"></i>
  </a>
  <script src="/assets/js/jquery-3.4.1.min.js"></script>
  <script src="/assets/js/popper-1.16.0.min.js"></script>
  <script src="/assets/js/bootstrap.min.js"></script>
  <script>
    $(window).on('load', function () {
      $("body").addClass("loaded");
    });
  </script>
  <script src="/assets/js/swiper-bundle.min.js"></script>
  <script src="/assets/js/ytdefer.min.js"></script>
  <script src="/assets/js/script.js"></script>

  {!! $web->scripts !!}
  @stack("scripts")
</body>

</html>
@else
{!! $web->offline_template !!}
@endif
