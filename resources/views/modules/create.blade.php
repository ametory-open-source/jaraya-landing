@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'Create Module'])
<div class="container-fluid py-4">
    <div class="card">
        <div class="card-header">Create Module</div>
        <div class="card-body table-responsive">
            <form action="/modules" method="post" class="form">
                @csrf
                <input type="hidden" name="picture" value="{{ old('picture') }}" class="picture">
                <input type="hidden" name="icon" value="{{ old('icon') }}" class="icon">
                <div class="row mb-3">
                    <div id="dropzone" class="dropzone-wrapper dropzone col-md-8 offset-md-2">
                        <div class="dz-message needsclick">
                            <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span
                                    class="path2"></span></i>
                            <!--begin::Info-->
                            <div class="ms-4">
                                <h3 class="fs-5 fw-bold text-gray-900 mb-1">Drop files here or click to upload.</h3>
                            </div>
                            <!--end::Info-->
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2 offset-md-2"><label for="price">Type</label></div>
                    <div class="col-md-6">
                        <select name="type" id="" class="form-control">
                            <option value="FEATURE" {{ old('type') == 'FEATURE' ? 'SELECTED' : null }}>Feature</option>
                            <option value="GROWTH" {{ old('type') == 'GROWTH' ? 'SELECTED' : null }}>Growth</option>
                            <option value="STEP" {{ old('type') == 'STEP' ? 'SELECTED' : null }}>Step</option>
                            <option value="CLIENT" {{ old('type') == 'CLIENT' ? 'SELECTED' : null }}>Client</option>
                            <option value="CLIENT-LOGO" {{ old('type') == 'CLIENT-LOGO' ? 'SELECTED' : null }}>Client Logo</option>
                            <option value="QUESTION" {{ old('type') == 'QUESTION' ? 'SELECTED' : null }}>Question</option>
                            <option value="PRICING" {{ old('type') == 'PRICING' ? 'SELECTED' : null }}>Pricing</option>
                            <option value="SCREENSHOT" {{ old('type') == 'SCREENSHOT' ? 'SELECTED' : null }}>Screenshot</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2 offset-md-2"><label for="title">Title</label></div>
                    <div class="col-md-6">
                        <input type="text" value="{{ old('title') }}" name="title" id="" class="form-control" placeholder="Title">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2 offset-md-2"><label for="subtitle">Subtitle</label></div>
                    <div class="col-md-6">
                        <input type="text" value="{{ old('subtitle') }}" name="subtitle" id="" class="form-control" placeholder="Subtitle">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2 offset-md-2"><label for="price">Price</label></div>
                    <div class="col-md-6">
                        <input type="text" value="{{ old('price') }}" name="price" id="" class="form-control" placeholder="Price">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2 offset-md-2"><label for="price">Sort</label></div>
                    <div class="col-md-6">
                        <input type="number" name="sort" id="" class="form-control" placeholder="Sort"
                            value="{{ old('sort') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2 offset-md-2"><label for="link">Link</label></div>
                    <div class="col-md-6">
                        <input type="text" name="link" id="" class="form-control" placeholder="Link"
                            value="{{ old('link') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2 offset-md-2"><label for="description">Description</label></div>
                    <div class="col-md-6">
                        <textarea rows="9" name="description" id="" class="form-control"
                            placeholder="Description">{{ old('description') }}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2 offset-md-2">
                        <label for="price">Icon</label>
                        <i class="fa-3x fad {{ old('icon') }} icon-selected" style="--fa-secondary-opacity: 0.3; --fa-primary-color: grey; --fa-secondary-color: red;"></i>
                    </div>
                    <div class="col-md-6">
                        <div class="row icon-list">
                            @foreach (collect(config("landing.fa_icon")) as $i => $item)
                            <div class="d-flex p-2 flex-column justify-content-between col-md-2 text-center icon-wrapper {{ $i > 24 ? 'icon-hide' : 'icon-show'}}"
                                data-icon="{{$item}}">
                                <i class="fa-2x fad {{ $item }}"></i>
                                <span style="font-size: 8pt;">{{ $item }}</span>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2 offset-md-4">
                        <button type="submit" class="btn bg-gradient-primary mt-5" style="min-width: 160px">Save</button>
                    </div>
                </div>


            </form>
        </div>
    </div>
</div>
@push("scripts")
<script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
<script>
    Dropzone.autoDiscover = false;
    // The dropzone method is added to jQuery elements and can
    // be invoked with an (optional) configuration object.
    $(".dropzone").dropzone({
        url: "/file/upload" ,
        method: 'post',
	    headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        maxFiles: 1,
        maxFilesize: 10, // MB
        addRemoveLinks: true,
        accept: function(file, done) {
            done()
        },
        success: function(file, response) {

            $(".picture").val(response.file)
        },

    });

    $('.icon-wrapper').click(function(el) {
        $('.icon-selected').attr("class", "fad fa-3x icon-selected "+$(this).data('icon'))
        $(".icon").val($(this).data('icon'))
    })
</script>
@endpush
@push("css")
<link rel="stylesheet" href="/assets/css/all.min.css">
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
<style>


</style>
@endpush
@endsection
