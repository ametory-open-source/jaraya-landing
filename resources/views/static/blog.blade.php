@extends("layouts.landing")

@section('content')
<!--hero section start-->
{{-- <section class="hero blog_hero">
    <div class="hero__wrapper blog_hero__wrapper">
        <div class="container">
            <div class="row">
                <div>
                    <h1>Our blog.</h1>
                </div>
                <div>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li><a href="blog.html"><i class="fad fa-long-arrow-right"></i>Blog</a></li>
                    </ul>
                    <div class="icon">
                        <i class="fad fa-bullhorn"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!--hero section end-->

<!--blog single start-->
<section class="blog_single">
    <div class="blog_single__wrapper">
        <div class="container">
            @if($blog->feature_image)
            <div class="blog_single__thumbnail" style="margin-top:120px">
                <img src="{{ assetUrl($blog->feature_image) }}" alt="{{$blog->feature_image_caption}}"
                    title="{{$blog->feature_image_caption}}">
                <p style="font-size: 12pt;margin-top:10px; font-style:italic">{{$blog->feature_image_caption}}</p>
            </div>
            @endif
            <div class="blog_single__content" style="margin-top: {{ $blog->feature_image ? '0' : '160px' }}">
                <h1>{{ $blog->title }}</h1>
                <h4>{{ $blog->all_comments_published->count() }} <i class="fad fa-comment"></i><span>|</span>{{
                    $blog->created_at->format("d-m-Y") }}</h4>
                {!! $blog->description !!}
                {{-- <div class="social">
                    <ul>
                        <li class="facebook"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="twitter"><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li class="linkedin"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li class="youtube"><a href="#"><i class="fab fa-youtube"></i></a></li>
                    </ul>
                </div> --}}
            </div>
        </div>
        @if ($blog->comment_enabled)
        <div class="container">
            @if ($message = Session::get('success'))
            <div style="padding:60px; cursor: pointer;" class="alert-wrapper" data-bs-dismiss="alert"
                aria-label="Close">>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong style="font-size: 18pt;">{{ $message }}</strong>
                </div>
            </div>
            @endif


            @if ($message = Session::get('error'))
            <div style="padding:60px; cursor: pointer;" class="alert-wrapper" data-bs-dismiss="alert"
                aria-label="Close">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong style="font-size: 18pt;">{{ $message }}</strong>
                </div>
            </div>
            @endif
        </div>
        <div class="container">
            <div class="blog_single__comment">
                <h3>{{ $blog->all_comments_published->count() }} replies</h3>
                @foreach ($blog->comments_published as $item)
                <div class="blog_single__comment--single">
                    <div class="image">
                        <img src="{{ $item->avatar ?? '/img/theme/tim.png'}}" alt="image">
                    </div>
                    <div class="comment">
                        <h4 class="name">{{ $item->name }}</h4>
                        <h5 class="date">{{ $item->created_at->format("d-m-Y H:i")}}</h5>
                        <p class="paragraph dark">{{ $item->comment }}
                        </p>

                        <div class="reply-button">
                            <a href="#comment_form__wrapper" onclick="reply({{$item->id}})">REPLY</a>
                        </div>
                    </div>
                </div>
                @foreach ($item->replies as $reply)
                <div class="blog_single__comment--single reply">
                    <div class="image">
                        <img src="{{ $reply->avatar ?? '/img/theme/tim.png'}}" alt="image">
                    </div>
                    <div class="comment">
                        <h4 class="name">{{ $reply->name }}</h4>
                        <h5 class="date">{{ $reply->created_at->format("d-m-Y H:i")}}</h5>
                        <p class="paragraph dark">{{ $reply->comment }}
                        </p>

                        <div class="reply-button">
                            <a href="#comment_form__wrapper" onclick="reply({{$item->id}})">REPLY</a>
                        </div>
                    </div>
                </div>
                @endforeach
                @endforeach

            </div>
            <div class="comment_form__wrapper" id="comment_form__wrapper">
                <form action="/comments/{{$blog->id}}" method="post">
                    @csrf
                    <h3>Write a reply</h3>
                    <input type="hidden" id="parent_id" name="parent_id" value="">
                    <div class="comment_form">
                        <div>
                            <input value="{{ old('name') }}" type="text" name="name" required placeholder="Name *" class="input-field">
                            <input value="{{ old('email') }}" type="email" name="email" required placeholder="Email *" class="input-field">
                            <input value="{{ old('avatar') }}" type="text" name="avatar" placeholder="Avatar URL" class="input-field">
                        </div>
                        <div>
                            <textarea name="comment" required placeholder="Write reply *"
                                class="input-field">{{ old('comment') }}</textarea>
                            <button class="button" type="submit"><span>POST REPLY <i
                                        class="fad fa-long-arrow-right"></i></span></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @endif


    </div>
</section>
<!--blog single end-->
@endsection
@push("scripts")
<script>
    function reply(comment_id) {
            $("#parent_id").val(comment_id)
        }
    $(".alert-wrapper").click(function() {
        $(this).remove()
    })
</script>
@endpush