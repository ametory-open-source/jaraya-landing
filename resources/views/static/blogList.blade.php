@extends("layouts.landing")

@section('content')
<section class="hero blog_hero">
    <div class="hero__wrapper blog_hero__wrapper">
        <div class="container">
            <div class="row">
                <div>
                    <h1>Our blog.</h1>
                </div>
                <div>
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li><a href="/blog"><i class="fad fa-long-arrow-right"></i>Blog</a></li>
                    </ul>
                    <div class="icon">
                        <i class="fad fa-newspaper"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="blog">
    <div class="blog__wrapper">
        <div class="container">
            <div class="blog__header">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="category__dropdown dropdown-wrapper">
                            <div class="category__dropdown-info dropdown-info">
                                <h6>Category</h6>
                                <i class="fad fa-angle-double-down"></i>
                            </div>
                            <div class="category__dropdown-box dropdown-box">
                                <ul>
                                    <li onclick="location.href= '/blog'">All</li>
                                    @foreach ($categories as $item)
                                        <li onclick="location.href= '/blog?category={{ $item->name }}'">{{ $item->name }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-lg-3">
                        <div class="date__dropdown dropdown-wrapper">
                            <div class="date__dropdown-info dropdown-info">
                                <h6>Date</h6>
                                <i class="fad fa-angle-double-down"></i>
                            </div>
                            <div class="date__dropdown-box dropdown-box">
                                <ul>
                                    <li>December 2020</li>
                                    <li>November 2020</li>
                                    <li>October 2020</li>
                                    <li>September 2020</li>
                                    <li>August 2020</li>
                                </ul>
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-lg-4">
                        <div class="search">
                            <form action="" method="get">
                            <input name="search" type="text" placeholder="Search" class="input-field input-search" value="{{request()->search}}">
                            <i class="fad fa-search"></i>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="blog__content">
            <div class="container">
                <div class="row">
                    @foreach ($blogs as $key => $item)
                    @php
                        $class = "col-lg-4";
                    @endphp
                    @switch(fmod($key + 1, 4))
                        @case(2)
                        @php
                            $class = "col-lg-8";
                        @endphp
                            @break
                        @case(3)
                        @php
                            $class = "col-lg-8 left";
                        @endphp
                        
                            @break
                        @default
                    @endswitch
                    <div class="{{ $class }}">
                        <a href="/blog/{{ $item->slug}}">
                            <div class="blog__single blog__single--{{ $key+1 }}">
                                <div class="blog__single-image">
                                    <img src="{{ assetUrl($item->feature_image) }}" alt="image">
                                </div>
                                <div class="blog__single-info">
                                    <h3>{{ $item->title }}</h3>
                                    <h4>{{ $item->all_comments_published->count() }} <i class="fad fa-comment"></i><span>|</span>{{
                                        $item->created_at->format("d-m-Y") }}</h4>
                                    <p class="paragraph dark">{{ $item->short_description}} </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
        <a href="#" class="button">
            <span>LOAD MORE <i class="fad fa-long-arrow-right"></i></span>
        </a>
    </div>
</section>
<!--blog single end-->
@endsection
@push("scripts")
<script>
    function(ev) {

    }
</script>
@endpush