@extends("layouts.landing")

@section('content')

<section class="hero blog_hero">
    <div class="hero__wrapper blog_hero__wrapper">
        <div class="container">
            <div class="row">
                <div>
                    <h1>Contact us.</h1>
                </div>
                <div>
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li><a href="/contact"><i class="fad fa-long-arrow-right"></i>Contact</a></li>
                    </ul>
                    <div class="icon">
                        <i class="fad fa-address-book"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@if($web->company_map)
<section class="map">
    <div class="map_wrapper">
        {!! $web->company_map !!}
    </div>
</section>
@endif
<section class="contact">
    <div class="container mt-5">
        <div class="row">
            <div class="col-4">
                <address>
                    <h2>{{ $web->company_name }}</h2>
                    <h4><i class="fas fa-map"></i> {{ $web->company_address }}</h4>
                    <h4><i class="fas fa-at"></i> {{ $web->company_email }}</h4>
                    <h4><i class="fas fa-phone"></i> {{ $web->company_phone }}</h4>
                </address>
            </div>
            <div class="col-8">
                <div class="comment_form__wrapper" id="comment_form__wrapper">
                    <form action="/contact" method="post">
                        @csrf
                        <div class="comment_form">
                            <div>
                                <input value="{{ old('name') }}" type="text" name="name" required placeholder="Name *"
                                    class="input-field">
                                <input value="{{ old('email') }}" type="email" name="email" required placeholder="Email *"
                                    class="input-field">
                                <input value="{{ old('phone') }}" type="text" name="phone" placeholder="Phone"
                                    class="input-field">
                            </div>
                            <div>
                                <textarea name="description" required placeholder="ex: what is jaraya? *"
                                    class="input-field">{{ old('description') }}</textarea>
                                <button class="button" type="submit"><span>Send <i
                                            class="fad fa-long-arrow-right"></i></span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </div>
</section>

<!--blog single end-->
@endsection
@push("scripts")
<script>
    @if ($message = Session::get('success'))
        alert("Thank you for your participation")
    @endif
    @if ($message = Session::get('error'))
        alert("Something error, we cannot receive your message")
    @endif
   
</script>
@endpush
@push('css')
    <style>
        .map_wrapper {
            width: calc(100% - 10rem);
            margin: 0 auto;
        }
        .map_wrapper iframe {
            border-radius: 3rem;
        }
        address .fas {
            color: #999;
        }
    </style>
@endpush