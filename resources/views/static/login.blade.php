@extends("layouts.landing")

@section('content')

<section id="login" class="container">
    <div class="row">
        <div class="col-md-12">
            <form action="/auth/login" method="post">
                @csrf

                <div class="input-wrapper">
                    <h1 class="section-heading color-black">Login to Jaraya App</h1>
                </div>
                <div class="input-wrapper">
                    <input value="{{ old('email') }}" type="email" name="email" required placeholder="Email *"
                        class="input-field">
                </div>
                <div class="input-wrapper">
                    <input type="password" value="{{ old('password') }}" type="text" name="password" required
                    placeholder="Password *" class="input-field">
                </div>
                <div class="input-wrapper">
                    <button class="button" type="submit"><span>Login <i
                        class="fad fa-long-arrow-right"></i></span></button>
                </div>
            </form>
        </div>
    </div>
</section>

<!--blog single end-->
@endsection
@push("scripts")
<script>
    @if ($message = Session::get('error'))
        alert("Something error, we cannot receive your message")
    @endif
    @if ($message = Session::get('redirect'))
        window.open('{!! $message !!}')
    @endif
</script>
@endpush
@push('css')
<style>
    .map_wrapper {
        width: calc(100% - 10rem);
        margin: 0 auto;
    }

    .map_wrapper iframe {
        border-radius: 3rem;
    }

    address .fas {
        color: #999;
    }

    #login {
        margin-top: 300px
    }

    .input-field {
        width: 50%;
        margin-bottom: 20px;
    }

    .input-wrapper button {
        width: 50%;
        margin-bottom: 20px;
    }

    .input-wrapper {
        display: flex;
        justify-content: center;
    }
    .section-heading {
        text-align: center;
        margin-bottom: 40px;
    }
</style>
@endpush