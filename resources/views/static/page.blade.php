@extends("layouts.landing")

@section('content')
<!--hero section start-->
{{-- <section class="hero blog_hero">
    <div class="hero__wrapper blog_hero__wrapper">
        <div class="container">
            <div class="row">
                <div>
                    <h1>Our blog.</h1>
                </div>
                <div>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li><a href="blog.html"><i class="fad fa-long-arrow-right"></i>Blog</a></li>
                    </ul>
                    <div class="icon">
                        <i class="fad fa-bullhorn"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!--hero section end-->

<!--blog single start-->
<section class="blog_single">
    <div class="blog_single__wrapper">
        <div class="container">
            @if($page->feature_image)
            <div class="blog_single__thumbnail" style="margin-top:120px">
                <img src="{{ assetUrl($page->feature_image) }}" alt="{{$page->feature_image_caption}}" title="{{$page->feature_image_caption}}">
                <p style="font-size: 12pt;margin-top:10px; font-style:italic">{{$page->feature_image_caption}}</p>
            </div>
            @endif
            <div class="blog_single__content" style="margin-top: {{ $page->feature_image ? '0' : '160px' }}">

                {{-- <h4>12 <i class="fad fa-comment"></i><span>|</span>Dec 17,2020</h4> --}}
                {!! $page->description !!}
                {{-- <div class="social">
                    <ul>
                        <li class="facebook"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="twitter"><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li class="linkedin"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li class="youtube"><a href="#"><i class="fab fa-youtube"></i></a></li>
                    </ul>
                </div> --}}
            </div>
        </div>

    </div>
</section>
<!--blog single end-->
@endsection
