@extends("layouts.landing")

@section('content')

<section id="content">
    <div class="container">
        {!! $static->content !!}
    </div>
</section>


@endsection
@push("css")
<style>
    {!! $static->styles !!}
</style>
@endpush
@push("scripts")
<script>
    {!! $static->scripts !!}
</script>
@endpush