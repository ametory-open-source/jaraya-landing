@extends("layouts.app", ['class' => 'g-sidenav-show bg-gray-100'])

@php
$pictures = [
"logo",
"loading",
"hero_picture",
"video_picture",
"question_picture",
"subscribe_picture",
];
@endphp
@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'Template'])
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Manage Template</h3>
        </div>
        <div class="card-body table-responsive" style="padding: 40px">
            @form_open([
            "class" => 'form',
            'enctype' => 'multipart/form-data'
            ])
            <div class="nav-wrapper position-relative end-0">
                <ul class="nav nav-pills nav-fill p-1" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link mb-0 px-0 py-1 active" data-bs-toggle="tab" href="#app-tabs-simple"
                            role="tab" aria-controls="app" aria-selected="true">
                            <i class="fas fa-rocket"></i> App
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mb-0 px-0 py-1" data-bs-toggle="tab" href="#company-tabs-simple"
                            role="tab" aria-controls="company" aria-selected="false">
                            <i class="fas fa-building"></i> Company
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mb-0 px-0 py-1" data-bs-toggle="tab" href="#template-tabs-simple" role="tab"
                            aria-controls="template" aria-selected="false">
                          <i class="fas fa-draw-polygon"></i>  Template
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mb-0 px-0 py-1" data-bs-toggle="tab" href="#picture-tabs-simple" role="tab"
                            aria-controls="picture" aria-selected="false">
                           <i class="fas fa-images"></i> Picture
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mb-0 px-0 py-1" data-bs-toggle="tab" href="#module-tabs-simple" role="tab"
                            aria-controls="module" aria-selected="false">
                          <i class="fas fa-box-open"></i>  Module
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="app-tabs-simple" role="tabpanel"
                        aria-labelledby="app-tabs-simple-tab">
                        {{-- input name --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="name">Name</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="" aria-describedby="name"
                                    placeholder="App Name" value="{!! $web->name  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input name --}}


                        {{-- input short_description --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="short_description">Short Description</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="short_description" id=""
                                    aria-describedby="short_description" placeholder="App Short Description"
                                    value="{!! $web->short_description  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input short_description --}}
                        {{-- input description --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="description"> Description</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="description" rows="5" id=""
                                    aria-describedby="description"
                                    placeholder="App  Description">{!! $web->description ?? '' !!}</textarea>
                            </div>
                        </div>
                        {{-- end input description --}}
                        {{-- input meta --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="meta">Meta</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="meta" rows="5" id=""
                                    aria-describedby="meta" placeholder="App Meta">{!! $web->meta ?? '' !!}</textarea>
                            </div>
                        </div>
                        {{-- end input meta --}}
                        {{-- input styles --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="styles">Styles</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="styles" rows="5" id=""
                                    aria-describedby="styles"
                                    placeholder="App Styles">{!! $web->styles ?? '' !!}</textarea>
                            </div>
                        </div>
                        {{-- end input styles --}}
                        {{-- input scripts --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="scripts">Scripts</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="scripts" rows="5" id=""
                                    aria-describedby="scripts"
                                    placeholder="App Scripts">{!! $web->scripts  ?? '' !!}</textarea>
                            </div>
                        </div>
                        {{-- end input scripts --}}
                    </div>
                    <div class="tab-pane fade" id="company-tabs-simple" role="tabpanel"
                        aria-labelledby="company-tabs-simple-tab">
                        <div class="form-group row d-flex">
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_name">Company Name</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="company_name" id=""
                                        aria-describedby="company_name" placeholder="App Company Name"
                                        value="{!! $web->company_name  ?? '' !!}">
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_phone">Company Phone</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="company_phone" id=""
                                        aria-describedby="company_phone" placeholder="App Company Phone"
                                        value="{!! $web->company_phone  ?? '' !!}">
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_email">Company Email</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="company_email" id=""
                                        aria-describedby="company_email" placeholder="App Company Email"
                                        value="{!! $web->company_email  ?? '' !!}">
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_web">Company Web</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="company_web" id=""
                                        aria-describedby="company_web" placeholder="App Company Web"
                                        value="{!! $web->company_web  ?? '' !!}">
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_coordinate">Company Location Coordinate</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="company_coordinate" id=""
                                        aria-describedby="company_coordinate" placeholder="App Company Location Coordinate"
                                        value="{!! $web->company_coordinate  ?? '' !!}">
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_map">Company Location Map</label>
                                </div>
                                <div class="col-sm-10">
                                    <textarea rows="9" type="text" class="form-control" name="company_map" id=""
                                        aria-describedby="company_map" placeholder="App Company Location Map"
                                        >{!! $web->company_map  ?? '' !!}</textarea>
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_address">Company Address</label>
                                </div>
                                <div class="col-sm-10">
                                    <textarea type="text" class="form-control" name="company_address" rows="9" id=""
                                        aria-describedby="company_address" placeholder="App Company Address"
                                        >{!! $web->company_address  ?? '' !!}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="template-tabs-simple" role="tabpanel"
                        aria-labelledby="template-tabs-simple-tab">

                        {{-- input playstore_link --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="playstore_link">Playstore Link</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="playstore_link" id=""
                                    aria-describedby="playstore_link" placeholder="App Playstore Link"
                                    value="{!! $web->playstore_link  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input playstore_link --}}
                        {{-- input appstore_link --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="appstore_link">Appstore Link</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="appstore_link" id=""
                                    aria-describedby="appstore_link" placeholder="App Appstore Link"
                                    value="{!! $web->appstore_link  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input appstore_link --}}
                        {{-- input hero_title --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="hero_title">Hero Title</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="hero_title" id=""
                                    aria-describedby="hero_title" placeholder="App Hero Title"
                                    value="{!! $web->hero_title  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input hero_title --}}
                        {{-- input hero_description --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="hero_description">Hero Description</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="hero_description" rows="5" id=""
                                    aria-describedby="hero_description"
                                    placeholder="App Hero Description">{!! $web->hero_description ?? '' !!}</textarea>
                            </div>
                        </div>
                        {{-- end input hero_description --}}
                        {{-- input feature_title --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="feature_title">Feature Title</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="feature_title" id=""
                                    aria-describedby="feature_title" placeholder="App Feature Title"
                                    value="{!! $web->feature_title  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input feature_title --}}
                        {{-- input video_link --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="video_link">Video Link</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="video_link" id=""
                                    aria-describedby="video_link" placeholder="App Video Link"
                                    value="{!! $web->video_link  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input video_link --}}
                        {{-- input growth_title --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="growth_title">Growth Title</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="growth_title" id=""
                                    aria-describedby="growth_title" placeholder="App Growth Title"
                                    value="{!! $web->growth_title  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input growth_title --}}
                        {{-- input growth_started_link --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="growth_started_link">Growth Started Link</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="growth_started_link" id=""
                                    aria-describedby="growth_started_link" placeholder="App Growth Started Link"
                                    value="{!! $web->growth_started_link  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input growth_started_link --}}
                        {{-- input growth_learn_link --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="growth_learn_link">Growth Learn Link</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="growth_learn_link" id=""
                                    aria-describedby="growth_learn_link" placeholder="App Growth Learn Link"
                                    value="{!! $web->growth_learn_link  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input growth_learn_link --}}
                        {{-- input step_title --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="step_title">Step Title</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="step_title" id=""
                                    aria-describedby="step_title" placeholder="App Step Title"
                                    value="{!! $web->step_title  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input step_title --}}
                        {{-- input step_started_link --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="step_started_link">Step Started Link</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="step_started_link" id=""
                                    aria-describedby="step_started_link" placeholder="App Step Started Link"
                                    value="{!! $web->step_started_link  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input step_started_link --}}
                        {{-- input step_learn_link --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="step_learn_link">Step Learn Link</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="step_learn_link" id=""
                                    aria-describedby="step_learn_link" placeholder="App Step Learn Link"
                                    value="{!! $web->step_learn_link  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input step_learn_link --}}

                        {{-- input client_title --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="client_title">Client Title</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="client_title" id=""
                                    aria-describedby="client_title" placeholder="App Client Title"
                                    value="{!! $web->client_title  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input client_title --}}
                        {{-- input client_subtitle --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="client_subtitle">Client Title</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="client_subtitle" id=""
                                    aria-describedby="client_subtitle" placeholder="App Client Title"
                                    value="{!! $web->client_subtitle  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input client_subtitle --}}
                        {{-- input client_description --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="client_description">Client Description</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="client_description" rows="5" id=""
                                    aria-describedby="client_description"
                                    placeholder="App Client Description">{!! $web->client_description ?? '' !!}</textarea>
                            </div>
                        </div>
                        {{-- end input client_description --}}
                        {{-- input question_title --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="question_title">Question Title</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="question_title" id=""
                                    aria-describedby="question_title" placeholder="App Question Title"
                                    value="{!! $web->question_title  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input client_title --}}
                        {{-- input screenshot_title --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="pricing_title">Pricing Title</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="pricing_title" id=""
                                    aria-describedby="pricing_title" placeholder="App Pricing Title"
                                    value="{!! $web->pricing_title  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input client_title --}}
                        {{-- input screenshot_title --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="screenshot_title">Screenshot Title</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="screenshot_title" id=""
                                    aria-describedby="screenshot_title" placeholder="App Screenshot Title"
                                    value="{!! $web->screenshot_title  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input screenshot_title --}}
                        {{-- input blog_title --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="blog_title">Blog Title</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="blog_title" id=""
                                    aria-describedby="blog_title" placeholder="App Blog Title"
                                    value="{!! $web->blog_title  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input blog_title --}}
                        {{-- input blog_rss_url --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="blog_rss_url">Blog RSS URL</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="blog_rss_url" id=""
                                    aria-describedby="blog_rss_url" placeholder="App Blog RSS URL"
                                    value="{!! $web->blog_rss_url  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input blog_rss_url --}}
                        {{-- input blog_url --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="blog_url">Blog URL</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="blog_url" id=""
                                    aria-describedby="blog_url" placeholder="App Blog URL"
                                    value="{!! $web->blog_url  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input blog_url --}}
                        {{-- input newsletter_title --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="newsletter_title">Newsletter Title</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="newsletter_title" id=""
                                    aria-describedby="newsletter_title" placeholder="App Newsletter Title"
                                    value="{!! $web->newsletter_title  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input newsletter_title --}}
                        {{-- input copyright --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="copyright">Copyright</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="copyright" id=""
                                    aria-describedby="copyright" placeholder="App Copyright"
                                    value="{!! $web->copyright  ?? '' !!}">
                            </div>
                        </div>

                        {{-- input header_menu --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="header_menu"> Header Menu</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="header_menu" rows="5" id=""
                                    aria-describedby="header_menu"
                                    placeholder="App  Header Menu">{!! $web->header_menu ?? '' !!}</textarea>
                            </div>
                        </div>
                        {{-- end input footer_explore_link --}}
                        {{-- input footer_explore_link --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="footer_explore_link"> Footer Explore Link</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="footer_explore_link" rows="5" id=""
                                    aria-describedby="footer_explore_link"
                                    placeholder="App  Footer Explore Link">{!! $web->footer_explore_link ?? '' !!}</textarea>
                            </div>
                        </div>
                        {{-- end input footer_explore_link --}}
                        {{-- input footer_help_link --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="footer_help_link"> Footer Help</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="footer_help_link" rows="5" id=""
                                    aria-describedby="footer_help_link"
                                    placeholder="App  Footer Help">{!! $web->footer_help_link ?? '' !!}</textarea>
                            </div>
                        </div>
                        {{-- end input offline_template --}}
                        {{-- input offline_template --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="offline_template"> Offline Template</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="offline_template" rows="5" id="offline_template"
                                    aria-describedby="offline_template"
                                    placeholder="App  Offline Template">{!! $web->offline_template ?? '' !!}</textarea>
                            </div>
                        </div>
                        {{-- end input offline_template --}}
                        <div class="col-md-3 mt-3">
                            <div class="form-check form-switch">
                                <input class="form-check-input" name="is_online" type="checkbox" role="switch"
                                    id="is_online" {{ $web->is_online ? 'CHECKED' : null}}>
                                <label class="form-check-label" for="is_online">Online</label>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="picture-tabs-simple" role="tabpanel"
                        aria-labelledby="picture-tabs-simple-tab">
                        @foreach ($pictures as $pic)
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="{!! $pic !!}">{{ str_replace("_", " ", Str::ucfirst($pic)) }}</label>


                            </div>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" name="{!! $pic !!}" id=""
                                    aria-describedby="{!! $pic !!}" placeholder="App {{ Str::ucfirst($pic) }}">
                                @if ($web[$pic])
                                <div class="img-preview-wrapper mt-2">
                                    <img src="/storage{{ $web[$pic] }}" alt="">
                                </div>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="tab-pane fade" id="module-tabs-simple" role="tabpanel"
                        aria-labelledby="module-tabs-simple-tab">
                        <div class="row">
                            <div class="col-md-3 mt-3">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="show_header" type="checkbox" role="switch"
                                        id="show_header" {{ $web->show_header ? 'CHECKED' : null}}>
                                    <label class="form-check-label" for="show_header">Show Header</label>
                                </div>
                            </div>
                            <div class="col-md-3 mt-3">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="show_hero" type="checkbox" role="switch"
                                        id="show_hero" {{ $web->show_hero ? 'CHECKED' : null}}>
                                    <label class="form-check-label" for="show_hero">Show Hero</label>
                                </div>
                            </div>
                            <div class="col-md-3 mt-3">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="show_feature" type="checkbox" role="switch"
                                        id="show_feature" {{ $web->show_feature ? 'CHECKED' : null}}>
                                    <label class="form-check-label" for="show_feature">Show Feature</label>
                                </div>
                            </div>
                            <div class="col-md-3 mt-3">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="show_video" type="checkbox" role="switch"
                                        id="show_video" {{ $web->show_video ? 'CHECKED' : null}}>
                                    <label class="form-check-label" for="show_video">Show Video</label>
                                </div>
                            </div>
                            <div class="col-md-3 mt-3">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="show_growth" type="checkbox" role="switch"
                                        id="show_growth" {{ $web->show_growth ? 'CHECKED' : null}}>
                                    <label class="form-check-label" for="show_growth">Show Growth</label>
                                </div>
                            </div>
                            <div class="col-md-3 mt-3">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="show_step" type="checkbox" role="switch"
                                        id="show_step" {{ $web->show_step ? 'CHECKED' : null}}>
                                    <label class="form-check-label" for="show_step">Show Step</label>
                                </div>
                            </div>
                            <div class="col-md-3 mt-3">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="show_client" type="checkbox" role="switch"
                                        id="show_client" {{ $web->show_client ? 'CHECKED' : null}}>
                                    <label class="form-check-label" for="show_client">Show Client</label>
                                </div>
                            </div>
                            <div class="col-md-3 mt-3">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="show_question" type="checkbox" role="switch"
                                        id="show_question" {{ $web->show_question ? 'CHECKED' : null}}>
                                    <label class="form-check-label" for="show_question">Show Question</label>
                                </div>
                            </div>
                            <div class="col-md-3 mt-3">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="show_pricing" type="checkbox" role="switch"
                                        id="show_pricing" {{ $web->show_pricing ? 'CHECKED' : null}}>
                                    <label class="form-check-label" for="show_pricing">Show Pricing</label>
                                </div>
                            </div>
                            <div class="col-md-3 mt-3">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="show_screenshot" type="checkbox" role="switch"
                                        id="show_screenshot" {{ $web->show_screenshot ? 'CHECKED' : null}}>
                                    <label class="form-check-label" for="show_screenshot">Show Screenshot</label>
                                </div>
                            </div>
                            <div class="col-md-3 mt-3">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="show_blog" type="checkbox" role="switch"
                                        id="show_blog" {{ $web->show_blog ? 'CHECKED' : null}}>
                                    <label class="form-check-label" for="show_blog">Show Blog</label>
                                </div>
                            </div>
                            <div class="col-md-3 mt-3">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="show_newsletter" type="checkbox" role="switch"
                                        id="show_newsletter" {{ $web->show_newsletter ? 'CHECKED' : null}}>
                                    <label class="form-check-label" for="show_newsletter">Show Newsletter</label>
                                </div>
                            </div>
                            <div class="col-md-3 mt-3">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="show_footer" type="checkbox" role="switch"
                                        id="show_footer" {{ $web->show_footer ? 'CHECKED' : null}}>
                                    <label class="form-check-label" for="show_footer">Show Footer</label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


           


            <button type="submit" class="btn bg-gradient-primary mt-5" style="min-width: 160px">Save</button>

            @form_close()
        </div>
    </div>
</div>
</main>
@push('css')
<style>
    .img-preview-wrapper {
        background-color: #f5f5f5;
        padding: 10px;
        margin: auto;
        text-align: center;
        border-radius: 10px;
    }

    .img-preview-wrapper img {
        max-height: 300px;
        min-height: 60px;
    }
    .tab-pane {
        padding: 40px 20px;
    }
    .nav-item .fas {
        color: #999;
        font-size: 10pt;
        margin-right: 10px;
    }
</style>
@endpush
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/codemirror.min.js" integrity="sha512-sSWQXoxIkE0G4/xqLngx5C53oOZCgFRxWE79CvMX2X0IKx14W3j9Dpz/2MpRh58xb2W/h+Y4WAHJQA0qMMuxJg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/keymap/sublime.min.js" integrity="sha512-SV3qeFFtzcmGtUQPLM7HLy/7GKJ/x3c2PdiF5GZQnbHzIlI2q7r77y0IgLLbBDeHiNfCSBYDQt898Xp0tcZOeA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/htmlmixed/htmlmixed.min.js" integrity="sha512-HN6cn6mIWeFJFwRN9yetDAMSh+AK9myHF1X9GlSlKmThaat65342Yw8wL7ITuaJnPioG0SYG09gy0qd5+s777w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/xml/xml.min.js" integrity="sha512-LarNmzVokUmcA7aUDtqZ6oTS+YXmUKzpGdm8DxC46A6AHu+PQiYCUlwEGWidjVYMo/QXZMFMIadZtrkfApYp/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/css/css.min.js" integrity="sha512-rQImvJlBa8MV1Tl1SXR5zD2bWfmgCEIzTieFegGg89AAt7j/NBEe50M5CqYQJnRwtkjKMmuYgHBqtD1Ubbk5ww==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/javascript/javascript.min.js" integrity="sha512-Cbz+kvn+l5pi5HfXsEB/FYgZVKjGIhOgYNBwj4W2IHP2y8r3AdyDCQRnEUqIQ+6aJjygKPTyaNT2eIihaykJlw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/addon/hint/javascript-hint.min.js" integrity="sha512-omIxBxPdObb7b3giwJtPBiB86Mey/ds7qyKFcRiaLQgDxoSR+UgCYEFO7jRZzPOCZAICabGCraEhOSa71U1zFA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/addon/hint/css-hint.min.js" integrity="sha512-1BD3lo262s+DtDFoeD8+ssEL1zVJU8SHWWMtUyxfLqMV/jQDEnyQlS/CL3mD1kkLSv2hhYq1sdcA/zAPDz4JVA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/addon/hint/javascript-hint.min.js" integrity="sha512-omIxBxPdObb7b3giwJtPBiB86Mey/ds7qyKFcRiaLQgDxoSR+UgCYEFO7jRZzPOCZAICabGCraEhOSa71U1zFA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>



    window.onload = function() {
        $('a[data-bs-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href");
            if (target === "#template-tabs-simple") {
                    CodeMirror.fromTextArea(document.getElementById(`offline_template`),{
                        theme:  "monokai",
                        mode:  "htmlmixed",
                        htmlMode: true,
                        keyMap: "sublime",
                        indentUnit: 4,
                        tabSize: 4,
                        indentWithTabs: false,
                        lineNumbers: true,
                        autoCloseTags: true,
                        lineWrapping: true,
                        autofocus: true,
                        extraKeys: {"Ctrl-Space": "autocomplete"},
                    });
            }
        });


       
    }

  
  
</script>
@endpush
@push("css")
<link rel="stylesheet" href="/assets/css/all.min.css">
<link rel="stylesheet" href="/assets/css/bootstrap-tagsinput.css">
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/codemirror.min.css" integrity="sha512-uf06llspW44/LZpHzHT6qBOIVODjWtv4MxCricRxkzvopAlSWnTf6hpZTFxuuZcuNE9CBQhqE0Seu1CoRk84nQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/theme/monokai.min.css" integrity="sha512-R6PH4vSzF2Yxjdvb2p2FA06yWul+U0PDDav4b/od/oXf9Iw37zl10plvwOXelrjV2Ai7Eo3vyHeyFUjhXdBCVQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush
@endsection