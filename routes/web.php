<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserProfileController;
use App\Http\Controllers\ResetPassword;
use App\Http\Controllers\ChangePassword;
use App\Http\Controllers\LandingController;

// Route::get('/', function () {
// 	return view('home');
// });

Route::get('/tool/command', function () {
	switch (request()->get("q")) {
		case 'migrate':
			Artisan::call('migrate', [
				'--path' => 'database/migrations',
				'--database' => 'mysql',
				'--force' => true
			]);
			break;

		case 'seed':
			Artisan::call('db:seed', [
				'--database' => 'mysql',
				'--force' => true
			]);
			break;

		case 'link':
			Artisan::call('storage:link', []);
			break;

		default:
			# code...
			break;
	}

	$output = Artisan::output();
	\Log::info(json_encode($output));

	return view('tool', compact("output"));

});

Route::get('/', [LandingController::class, 'main'])->name('main');
Route::get('/join', [LandingController::class, 'join'])->name('join');
Route::get('/invitation', [LandingController::class, 'invitation'])->name('invitation');
Route::post('/join', [LandingController::class, 'post_join'])->name('join.post');
Route::get('/contact', [LandingController::class, 'contact'])->name('contact');
Route::get('/pricing', [LandingController::class, 'pricing'])->name('pricing');

Route::get('/p/{slug}', [LandingController::class, 'page'])->name('page');
Route::get('/blog', [LandingController::class, 'blogList'])->name('blogList');

Route::get('/auth/login', [LandingController::class, 'auth_login'])->name('auth.login');
Route::post('/auth/login', [LandingController::class, 'post_auth_login'])->name('auth.postLogin');
Route::get('/faq', [LandingController::class, 'faq'])->name('faq');
Route::post('/contact', [LandingController::class, 'contactSend'])->name('contactSend');
Route::get('/gallery', [LandingController::class, 'galleryList'])->name('galleryList');
Route::get('/gallery/{gallery}', [LandingController::class, 'gallery'])->name('gallery');
Route::get('/blog/{slug}', [LandingController::class, 'blog'])->name('blog');
Route::get('/galleries', [LandingController::class, 'galleries'])->name('galleries');
Route::post('/comments/{blog}', [LandingController::class, 'postComment'])->name('postComment');
Route::post('/newsletter', [LandingController::class, 'newsletter'])->name('newsletter');
Route::get('/register', [RegisterController::class, 'create'])->middleware('guest')->name('register');
Route::post('/register', [RegisterController::class, 'store'])->middleware('guest')->name('register.perform');
Route::get('/login', [LoginController::class, 'show'])->middleware('guest')->name('login');
Route::post('/login', [LoginController::class, 'login'])->middleware('guest')->name('login.perform');
Route::get('/reset-password', [ResetPassword::class, 'show'])->middleware('guest')->name('reset-password');
Route::post('/reset-password', [ResetPassword::class, 'send'])->middleware('guest')->name('reset.perform');
Route::get('/change-password', [ChangePassword::class, 'show'])->middleware('guest')->name('change-password');
Route::post('/change-password', [ChangePassword::class, 'update'])->middleware('guest')->name('change.perform');
Route::get('/dashboard', [HomeController::class, 'index'])->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
	Route::post('/file/upload', [LandingController::class, 'upload'])->name('upload');
	Route::get('/template', [LandingController::class, 'template'])->name('template');
	Route::post('/template', [LandingController::class, 'save_template'])->name('template.post');
	// Route::get('/virtual-reality', [PageController::class, 'vr'])->name('virtual-reality');
	// Route::get('/rtl', [PageController::class, 'rtl'])->name('rtl');
	// Route::get('/profile', [UserProfileController::class, 'show'])->name('profile');
	// Route::post('/profile', [UserProfileController::class, 'update'])->name('profile.update');
	// Route::get('/profile-static', [PageController::class, 'profile'])->name('profile-static');
	// Route::get('/sign-in-static', [PageController::class, 'signin'])->name('sign-in-static');
	// Route::get('/sign-up-static', [PageController::class, 'signup'])->name('sign-up-static');

	Route::post('logout', [LoginController::class, 'logout'])->name('logout');

	Route::get('/users', [App\Http\Controllers\UsersController::class, 'index'])->name('users.index');


	Route::get('/provinces', [App\Http\Controllers\ProvincesController::class, 'index'])->name('provinces.index');


	Route::get('/pages', [App\Http\Controllers\PagesController::class, 'index'])->name('pages.index');
	Route::get('/pages/create', [App\Http\Controllers\PagesController::class, 'create'])->name('pages.create');
	Route::post('/pages', [App\Http\Controllers\PagesController::class, 'store'])->name('pages.store');
	Route::get('/pages/{page}/edit', [App\Http\Controllers\PagesController::class, 'edit'])->name('pages.edit');
	Route::put('/pages/{page}/update', [App\Http\Controllers\PagesController::class, 'update'])->name('pages.update');
	Route::get('/pages/{page}/delete', [App\Http\Controllers\PagesController::class, 'delete'])->name('modules.delete');


	Route::get('/modules', [App\Http\Controllers\ModulesController::class, 'index'])->name('modules.index');
	Route::get('/modules/create', [App\Http\Controllers\ModulesController::class, 'create'])->name('modules.create');
	Route::post('/modules', [App\Http\Controllers\ModulesController::class, 'store'])->name('modules.store');
	Route::get('/modules/{module}/delete', [App\Http\Controllers\ModulesController::class, 'delete'])->name('modules.delete');
	Route::get('/modules/{module}/edit', [App\Http\Controllers\ModulesController::class, 'edit'])->name('modules.edit');
	Route::put('/modules/{module}/update', [App\Http\Controllers\ModulesController::class, 'update'])->name('modules.update');


	Route::get('/newsletters', [App\Http\Controllers\NewslettersController::class, 'index'])->name('newsletters.index');


	Route::get('/blogs', [App\Http\Controllers\BlogsController::class, 'index'])->name('blogs.index');
	Route::get('/blogs/create', [App\Http\Controllers\BlogsController::class, 'create'])->name('blogs.create');
	Route::get('/blogs/{blog}/edit', [App\Http\Controllers\BlogsController::class, 'edit'])->name('modules.edit');
	Route::put('/blogs/{blog}/update', [App\Http\Controllers\BlogsController::class, 'update'])->name('modules.update');
	Route::get('/blogs/{blog}/delete', [App\Http\Controllers\BlogsController::class, 'delete'])->name('modules.delete');


	Route::get('/blogs', [App\Http\Controllers\BlogsController::class, 'index'])->name('blogs.index');


	Route::get('/blogs', [App\Http\Controllers\BlogsController::class, 'index'])->name('blogs.index');
	Route::post('/blogs', [App\Http\Controllers\BlogsController::class, 'store'])->name('pages.store');


	Route::get('/blog_categories', [App\Http\Controllers\BlogCategoriesController::class, 'index'])->name('blog_categories.index');
	Route::get('/blog_categories/create', [App\Http\Controllers\BlogCategoriesController::class, 'create'])->name('blog_categories.create');
	Route::post('/blog_categories', [App\Http\Controllers\BlogCategoriesController::class, 'store'])->name('blog_categories.store');
	Route::get('/blog_categories/{blog_category}/edit', [App\Http\Controllers\BlogCategoriesController::class, 'edit'])->name('blog_categories.edit');
	Route::put('/blog_categories/{blog_category}/update', [App\Http\Controllers\BlogCategoriesController::class, 'update'])->name('blog_categories.update');
	Route::get('/blog_categories/{blog_category}/delete', [App\Http\Controllers\BlogCategoriesController::class, 'delete'])->name('blog_categories.delete');


	Route::get('/comments', [App\Http\Controllers\CommentsController::class, 'index'])->name('comments.index');
	Route::get('/comments/{comment}/edit', [App\Http\Controllers\CommentsController::class, 'edit'])->name('comments.edit');
	Route::post('/comments/{comment}/approval', [App\Http\Controllers\CommentsController::class, 'approval'])->name('comments.approval');
	Route::get('/comments/{comment}/delete', [App\Http\Controllers\CommentsController::class, 'delete'])->name('comments.delete');


	Route::get('/galleries', [App\Http\Controllers\GalleriesController::class, 'index'])->name('galleries.index');
	Route::get('/galleries/create', [App\Http\Controllers\GalleriesController::class, 'create'])->name('galleries.create');
	Route::post('/galleries', [App\Http\Controllers\GalleriesController::class, 'store'])->name('galleries.store');
	Route::get('/galleries/{gallery}/edit', [App\Http\Controllers\GalleriesController::class, 'edit'])->name('galleries.edit');
	Route::put('/galleries/{gallery}/update', [App\Http\Controllers\GalleriesController::class, 'update'])->name('galleries.update');
	Route::get('/galleries/{gallery}/cover', [App\Http\Controllers\GalleriesController::class, 'cover'])->name('galleries.cover');
	Route::get('/galleries/{gallery}/delete-item', [App\Http\Controllers\GalleriesController::class, 'deleteItem'])->name('galleries.deleteItem');



	Route::get('/contact_us', [App\Http\Controllers\ContactusesController::class, 'index'])->name('contact_us.index');


	Route::get('/statics', [App\Http\Controllers\StaticsController::class, 'index'])->name('statics.index');
	Route::get('/statics/create', [App\Http\Controllers\StaticsController::class, 'create'])->name('statics.create');
	Route::get('/statics/{static}/edit', [App\Http\Controllers\StaticsController::class, 'edit'])->name('statics.edit');
	Route::put('/statics/{static}/update', [App\Http\Controllers\StaticsController::class, 'update'])->name('statics.update');
	Route::get('/statics/{static}/delete', [App\Http\Controllers\StaticsController::class, 'delete'])->name('statics.delete');
	Route::post('/statics', [App\Http\Controllers\StaticsController::class, 'store'])->name('statics.store');


	// DON'T REMOVE THIS LINE




	//
});

Route::get('/{page}', [App\Http\Controllers\StaticsController::class, 'viewPage'])->name('static');
